import React, { useEffect } from 'react';
import Layout from "./components/Layout";
import { Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import PageOne from "./pages/PageOne";
import PageTwo from "./pages/PageTwo";
import PageThree from "./pages/PageThree";
import PageMain from "./pages/PageMain";
import ReactGA from 'react-ga';
const TRACKING_ID = "UA-226115372-3"; // OUR_TRACKING_ID

ReactGA.initialize(TRACKING_ID);

function App() {

    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
      }, []);

    const throwKnownError = () => {
        throw new Error("Sentry Error");
    }
    
    return (
        <>
        <div>
            <button onClick={throwKnownError}>Error Button</button>
        </div>
        <Layout>
            <Switch>
                <Route path="/" exact>
                    <Home />
                </Route>
                <Route path="/page-one">
                    <PageOne />
                </Route>
                <Route path="/page-two">
                    <PageTwo />
                </Route>
                <Route path="/page-three">
                    <PageThree />
                </Route>
                <Route path="/page-main">
                    <PageMain />
                </Route>
            </Switch>
        </Layout>
        </>
    );
}

export default App;
